#!/bin/bash

DOCKER_CONTAINER=$1

set +e

echo '----------------------------------------------------------'
echo "Killing and removing Docker containers ($DOCKER_CONTAINER)"
for i in $(docker ps -a | grep $DOCKER_CONTAINER | awk '{print $1}')
do
  docker kill $i; wait;
  docker rm -f $i; wait;
done;

echo '-------------------------------------'
echo "docker ps -a | grep $DOCKER_CONTAINER"
docker ps -a | grep $DOCKER_CONTAINER

set -e
